# 22 Summer - Frontend Study(React)

## 🗓️ 기간 : 2022.07.01 ~ 08.28

### 👩‍👧‍👦 Member

#### 👩‍👧‍👦 Team 유빈
<center>
<table  width="100%">
  <tr>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/106325839?v=4"  width="100px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/101553874?v=4"  width="100px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/96576724?v=4"  width="100px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/104255055?v=4"  width="100px;"  alt=""/>
    </td>
  </tr>
<tr>
  <td align="center">
    <a href="https://github.com/5uhwann">
      <div>수환</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/2SEONGA">
      <div>성아</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/yeil99">
      <div>예일</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/sjmd117">
      <div>주석</div>
    </a>
  </td>
</tr>
</table>
</center>

#### 👩‍👧‍👦 Team 지은
<center>
<table  width="100%">
  <tr>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/102955516?v=4"  width="100px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/81469686?v=4"  width="100px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/108217858?v=4"  width="100px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/108321588?v=4"  width="100px;"  alt=""/>
    </td>
  </tr>
<tr>
  <td align="center">
    <a href="https://github.com/xxxjinn">
      <div>현진</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/89882">
      <div>가현</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/keemsebin">
      <div>세빈</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/euntaek4187">
      <div>은택</div>
    </a>
  </td>
</tr>
</table>
</center>

#### 👩‍👧‍👦 Team 보겸
<center>
<table  width="100%">
  <tr>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/75975946?v=4"  width="80px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/101556294?v=4"  width="80px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/103632151?v=4"  width="80px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/108409327?v=4"  width="80px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/101553846?v=4"  width="80px;"  alt=""/>
    </td>
  </tr>
<tr>
  <td align="center">
    <a href="https://github.com/2020-nug">
      <div>유경</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/solmin12">
      <div>솔민</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/jsu5328">
      <div>소윤</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/YU-JIN-JUNG">
      <div>유진</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/yehoon2">
      <div>예훈</div>
    </a>
  </td>
</tr>
</table>
</center>

#### 👩‍👧‍👦 Team 민수
<center>
<table  width="100%">
  <tr>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/101553916?v=4"  width="100px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/87488288?v=4"  width="100px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/102343946?v=4"  width="100px;"  alt=""/>
    </td>
    <td  align="center">
      <img  src="https://avatars.githubusercontent.com/u/108217489?v=4"  width="100px;"  alt=""/>
    </td>
  </tr>
<tr>
  <td align="center">
    <a href="https://github.com/haejun1">
      <div>해준</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/asylee02">
      <div>상연</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/seojeahyean">
      <div>제현</div>
    </a>
  </td>
  <td align="center">
    <a href="https://github.com/JinseoHong0">
      <div>진서</div>
    </a>
  </td>
</tr>
</table>
</center>


### 🔥 목적

- 프론트엔드 개발에 대한 지식 습득


### ✍️ 진행 방식

- 주차 학습일 마다 학습 후 {이름}.md file에 정리하여 pr을 올린다.

- 마지막 학습일에는 모두가 모여 RandomBitFlip.md 에 최종 정리하여 작성 후 pr을 올린다.


### 학습 내용 보러 가기

**👶 1주차**

| Team 이름 | Github 링크 |
|:------:|:---------:|
| Team 유빈 | [team유빈-1주차](https://github.com/MJU-Coin/22-S-React/tree/main/1-week/team_youbin)|
| Team 지은 | [team지은-1주차](https://github.com/MJU-Coin/22-S-React/tree/main/1-week/team_jieun)|
| Team 보겸 | [team보겸-1주차](https://github.com/MJU-Coin/22-S-React/tree/main/1-week/team_bokyeom)|
| Team 민수 | [team민수-1주차](https://github.com/MJU-Coin/22-S-React/tree/main/1-week/team_minsu)|
